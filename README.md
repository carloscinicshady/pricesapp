# PricesApp
Prices App is a RESTful API Endpoint that allows users to retrieve prices for a specific product from different brands. If the date of the brand that the client is introducing it´s within the range of prices that are stored in the database, the API returns the price wich is stored in the DB and wich has the highest priority.

Technologies Used
Java 11
Spring Boot 2.5.0
Maven
PostgreSQL
