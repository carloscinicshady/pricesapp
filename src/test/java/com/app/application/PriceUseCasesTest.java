package com.app.application;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import com.app.infrastructure.persistance.PriceRepository;
import com.app.service.PriceUseCases;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PriceUseCasesTest {

    @Autowired
    PriceUseCases myUseCases;
    @Autowired
    PriceRepository myRepo;

    @BeforeEach
    public void init() {
        myUseCases = new PriceUseCases(myRepo);
    }

    @Test
    public void testPrice1() {
        ArrivingPriceDTO myArrival = new ArrivingPriceDTO();
        myArrival.setPriceDateTime(LocalDateTime.of(2020, 6, 14, 10, 00, 00));
        myArrival.setProductId(0);
        myArrival.setBrandId(0);

        List<PriceDTO> myPrices = myRepo.findPrices();
        ExitingPriceDTO myExit = myUseCases.showPrices(myPrices, myArrival);

        assertNotEquals(myExit, null);
    }

    @Test
    public void testPrice2() {
        ArrivingPriceDTO myArrival = new ArrivingPriceDTO();
        myArrival.setPriceDateTime(LocalDateTime.of(2020, 6, 14, 16, 00, 00));
        myArrival.setProductId(0);
        myArrival.setBrandId(0);

        List<PriceDTO> myPrices = myRepo.findPrices();
        ExitingPriceDTO myExit = myUseCases.showPrices(myPrices, myArrival);

        assertNotEquals(myExit, null);
    }

    @Test
    public void testPrice3() {
        ArrivingPriceDTO myArrival = new ArrivingPriceDTO();
        myArrival.setPriceDateTime(LocalDateTime.of(2020, 6, 14, 21, 00, 00));
        myArrival.setProductId(0);
        myArrival.setBrandId(0);

        List<PriceDTO> myPrices = myRepo.findPrices();
        ExitingPriceDTO myExit = myUseCases.showPrices(myPrices, myArrival);

        assertNotEquals(myExit, null);
    }

    @Test
    public void testPrice4() {
        ArrivingPriceDTO myArrival = new ArrivingPriceDTO();
        myArrival.setPriceDateTime(LocalDateTime.of(2020, 6, 15, 10, 00, 00));
        myArrival.setProductId(0);
        myArrival.setBrandId(0);

        List<PriceDTO> myPrices = myRepo.findPrices();
        ExitingPriceDTO myExit = myUseCases.showPrices(myPrices, myArrival);

        assertNotEquals(myExit, null);
    }

    @Test
    public void testPrice5() {
        ArrivingPriceDTO myArrival = new ArrivingPriceDTO();
        myArrival.setPriceDateTime(LocalDateTime.of(2020, 6, 16, 21, 00, 00));
        myArrival.setProductId(0);
        myArrival.setBrandId(0);

        List<PriceDTO> myPrices = myRepo.findPrices();
        ExitingPriceDTO myExit = myUseCases.showPrices(myPrices, myArrival);

        assertNotEquals(myExit, null);
    }

}
