package com.app.infrastructure.persistance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.application.PriceDTO;

import java.util.List;

public interface PriceRepository extends JpaRepository<PriceDTO, Long> {

    @Query(value = "SELECT * FROM prices ", nativeQuery = true)
    List<PriceDTO> findPrices();

}
