package com.app.infrastructure.controller;

import com.app.application.ArrivingPriceDTO;
import com.app.application.ExitingPriceDTO;
import com.app.application.PriceDTO;
import com.app.domain.util.PriceNotFoundException;
import com.app.service.PriceUseCases;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

@RestController
@RequestMapping("/prices")
public class PriceController {

    private final PriceUseCases myService;

    public PriceController(PriceUseCases myService) {
        this.myService = myService;
    }

    @GetMapping("/check")
    public ExitingPriceDTO findAllPrices(@RequestParam String priceTime, @RequestParam int priceProductid,
            @RequestParam int priceBrandId) {

        String pattern = "yyyy-MM-dd HH:mm:ss";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        LocalDateTime dateTime = null;
        try {
            dateTime = LocalDateTime.parse(priceTime, formatter);
        } catch (DateTimeParseException e) {
            System.out.println("Error parsing LocalDateTime: " + e.getMessage());
        }

        ArrivingPriceDTO myArrival = new ArrivingPriceDTO(dateTime, priceProductid,
                priceBrandId);
        List<PriceDTO> myList = myService.priceRepo.findPrices();

        ExitingPriceDTO myExit = myService.showPrices(myList, myArrival);

        if (myExit != null) {
            return myExit;
        } else {
            throw new PriceNotFoundException("Price not found for the given parameters");
        }
    }
}
