package com.app.service;

import com.app.application.ArrivingPriceDTO;
import com.app.application.ExitingPriceDTO;
import com.app.application.PriceDTO;
import com.app.infrastructure.persistance.PriceRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class PriceUseCases {

    @Autowired
    public PriceRepository priceRepo;

    public PriceUseCases(PriceRepository priceRepo) {
        this.priceRepo = priceRepo;

    }

    public ExitingPriceDTO showPrices(List<PriceDTO> myList, ArrivingPriceDTO myArrivingPrice) {

        PriceDTO comparedPrice = myList.stream()
                .filter(myPrice -> myPrice.getStartDate().isBefore(myArrivingPrice.getPriceDateTime()) &&
                        myPrice.getEndDate().isAfter(myArrivingPrice.getPriceDateTime()))
                .sorted(Comparator.comparing(PriceDTO::getPriority))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No matching price found"));

        ExitingPriceDTO myExitPrice = new ExitingPriceDTO(comparedPrice.getBrandId(), comparedPrice.getStartDate(),
                comparedPrice.getEndDate(),
                comparedPrice.getPriceList(), comparedPrice.getProductId(), comparedPrice.getPrice() + " " +
                        comparedPrice.getCurrency());

        return myExitPrice;
    }

}
