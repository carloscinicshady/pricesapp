package com.app.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.time.LocalDateTime;

@NoArgsConstructor
@Entity
@Data
@Table(name = "PRICES")
public class Price {

    private int brandId;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PRICE_LIST")
    private int priceList;
    private int productId;
    private int priority;
    private double price;
    private String currency;

}
