package com.app.domain.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FormatUtil {
    static String pattern = "yyyy-MM-dd HH:mm:ss";
    static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);

    public static LocalDateTime dateParser(String date) {
        return LocalDateTime.parse(date, formatter);
    }
}
